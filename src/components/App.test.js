import React from 'react';
import App from './App';
import renderer from 'react-test-renderer';

describe('App component', () => {
    it('renders correctly', () => {
        const tree = renderer.create(
            <App />
        ).toJSON();
        expect(tree.children.length).toBe(4);
        expect(tree).toMatchSnapshot();
    });
});