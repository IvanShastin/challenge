import React, { Component } from 'react';
import Game from './Game';

const styles = {
  games: {
    margin: '15px',
    fontSize: '14px',
    fontWeight: 'bold'
  },
  button: {
    fontSize: '14px',
    fontWeight: 'bold',
    margin: '15px'
  }
};


class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      game: '',
      games: [],
      stats: []
    }
  }

  handleChange = e => {
    e.preventDefault();
    const {value} = e.target;
    this.setState({
      game: value
    });
  } 

  handleClick = e => {
    e.preventDefault();
    const {game} = this.state;
    fetch(`/api/games/${game}`)
      .then(res => res.json())
      .then(stats => this.setState({
        stats: stats
      }))
      .catch(err => console.log(err));
  }

  async componentDidMount() {
    const response = await fetch('/api/games');
    const games = await response.json();
    this.setState({
      games: games
    })
  }

  render() {
    const {games, game, stats} = this.state;
    return (
      <div>
        <h1>Eredivisie 2017/2018</h1>
        <h2>Select game from the list below to see statistics</h2>
        <select style={styles.games} onChange={this.handleChange.bind(this)}>
          {
            games.map(game => <option key={game}>{game}</option>)
          }
        </select>
        <button 
          style={styles.button}
          disabled={game === ''} 
          onClick={this.handleClick.bind(this)}
          > Statistics
        </button>
        {
          stats.length > 0 && <Game stats={stats} game={game}/>
        }
      </div>
    );
  }
}

export default App;
