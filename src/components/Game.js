import React, { Component } from "react";
import Stats from './Stats';

const styles = {
    game: {
        fontSize: '14px',
        display: 'inline'
    },
    team1: {
        float: 'left',
        paddingLeft: '10px',
        paddingRight: '20px'
    },
    team2: {
        float: 'right',
        paddingRight: '10px'
    },
    score: {
        fontSize: '18px',
        fontWeight: 'bold'
    }
}

class Game extends Component {
    render() {
        const {game, stats} = this.props;
        const [team1, team2] = stats;
        return (
            <div style={styles.game}>
                <h3>Game {game} statistics</h3>
                <div style={styles.score}>
                    Score: {team1.goals.length} : {team2.goals.length}
                </div>
                <div style={styles.team1}>
                    <h2>{team1.team}</h2>
                    <Stats stats={team1} />
                </div>
                <div>
                    <h2>{team2.team}</h2>
                    <Stats stats={team2} />
                </div>
            </div>
        )
    }
}

export default Game;