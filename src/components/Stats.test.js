import React from 'react';
import Stats from './Stats';
import renderer from 'react-test-renderer';

const testProps = {
    game: '2174508',
    stats: [{goals: ['1', '2', '3'],},{goals: ['1', '2']}]
}

describe('Stats component', () => {

    it('renders correctly', () => {
        const tree = renderer.create(
            <Game {...testProps}/>
        ).toJSON();
        expect(tree).toMatchSnapshot();
    });
});

const Stats = ({stats}) => {
    return (
        <div>
            Goals: {stats.goals.length} <br />
            Corners: {stats.corners} <br />
            Saves: {stats.saves} <br />
            Fouls: {stats.fouls} <br />
            Goal kicks: {stats.goalKicks} <br />
            Offsides: {stats.offsides} <br />
            Shot wide: {stats.shotsWide} <br />
        </div>
    )
}