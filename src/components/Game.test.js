import React from 'react';
import Game from './Game';
import renderer from 'react-test-renderer';

const testProps = {
    game: '2174508',
    stats: [{goals: ['1', '2', '3'],},{goals: ['1', '2']}]
}

describe('Game component', () => {

    it('renders correctly', () => {
        const tree = renderer.create(
            <Game {...testProps}/>
        ).toJSON();
        expect(tree).toMatchSnapshot();
    });
});