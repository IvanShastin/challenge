import React from 'react';

const Stats = ({stats}) => {
    return (
        <div>
            Goals: {stats.goals.length} <br />
            Corners: {stats.corners} <br />
            Saves: {stats.saves} <br />
            Fouls: {stats.fouls} <br />
            Goal kicks: {stats.goalKicks} <br />
            Offsides: {stats.offsides} <br />
            Shot wide: {stats.shotsWide} <br />
        </div>
    );
}

export default Stats;
