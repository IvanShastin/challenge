const fs = require('fs');
const parse = require('csv-parse');
const path = require('path');
const util = require('util');

const readFileAsync = util.promisify(fs.readFile);
const parseAsync = util.promisify(parse);

const dataSet = path.resolve(__dirname, 'dataset.csv');

const loadGamesData = async () => {
    const file = await readFileAsync(dataSet);
    const games = await parseAsync(file, {columns: true});
    return games;
}

const getGames = (arr) => {
    const games = [];
    arr.forEach(element => {
        if(games.indexOf(element.n_Matchid) === -1)
            games.push(element.n_Matchid);
    });
    return games;
}

const getGameData = (matchId, dataset) => {
    return dataset.filter(item => item.n_Matchid === matchId);
}

const gameTeams = (gameData) => {
    const teams = [];
    gameData.forEach(item => {
        if(teams.indexOf(item.c_Team) === -1)
            teams.push(item.c_Team)
    });
    return teams.filter(team => team !== 'NULL');
}

const getTeamStatistics = (team, game) => {
    const goals = game.filter(
        item => item.c_Team === team && item.c_Action === 'Goal'
    );
    const saves = game.filter(
        item => item.c_Team === team && item.c_Action === 'Save by goalkeeper'
    ).length;
    const fouls = game.filter(
        item => item.c_Team === team && item.c_Action === 'Foul committed'
    ).length;
    const corners = game.filter(
        item => item.c_Team === team && item.c_Action === 'Corner'
    ).length;
    const offsides = game.filter(
        item => item.c_Team === team && item.c_Action === 'Offside'
    ).length;
    const goalKicks = game.filter(
        item => item.c_Team === team && item.c_Action === 'Goal kick'
    ).length;
    const shotsWide = game.filter(
        item => item.c_Team === team && item.c_Action === 'Shot wide'
    ).length;

    return {
        team: team,
        goals: goals,
        saves: saves,
        fouls: fouls,
        corners: corners,
        offsides: offsides,
        goalKicks: goalKicks,
        shotsWide: shotsWide
    }
}

module.exports = {
    loadGamesData,
    getGames,
    getGameData,
    getTeamStatistics,
    gameTeams
}