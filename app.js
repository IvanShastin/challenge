const express = require('express');
const app = express();
const gameApi = require('./gameApi');

app.use(express.static('public'));
app.set('view engine', 'ejs');

app.get('/', (req, res) => {
    res.render('index');
});

app.get('/api/games', async (req, res) => {
    const data = await gameApi.loadGamesData();
    const games = gameApi.getGames(data);
    res.json(games);
});

app.get('/api/games/:id', async (req, res) => {
    const gameId = req.params.id;
    const data = await gameApi.loadGamesData();
    const gameData = gameApi.getGameData(`${gameId}`, data);
    const teams = gameApi.gameTeams(gameData);
    const stats = teams.map(team => gameApi.getTeamStatistics(team, data));
    res.json(stats);
});

module.exports = app;
