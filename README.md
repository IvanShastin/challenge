# How to run

## Option 1:
npm install
npm start

Open browser and navigate to localhost:3000

##Option 2:
docker build -t challenge:1.0 .
docker run -d -p 3000:3000 challenge:1.0

Open browser and navigate to localhost:3000

##How to run tests
npm run test