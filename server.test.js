const request = require('supertest');
const app = require('./app')

describe('Test the root path', () => {
    test('It should response the GET method', async () => {
        const response = await request(app).get('/');
        expect(response.statusCode).toBe(200);
    });
});

describe('Test /api/games path', () => {
    test('It should response the GET method', async () => {
        const response = await request(app).get('/api/games');
        expect(response.statusCode).toBe(200);
    });
});

describe('Test /api/games/:id path', () => {
    const testId = '2174508'
    test('It should response the GET method', async () => {
        const response = await request(app).get(`/api/games/${testId}`);
        expect(response.statusCode).toBe(200);
    });
    test('It should respond with statistics', async () => {
        const response = await request(app).get(`/api/games/${testId}`);
        const stats = response.body;
        expect(stats).toBeDefined();
        expect(stats).toHaveLength(2);
    })
});